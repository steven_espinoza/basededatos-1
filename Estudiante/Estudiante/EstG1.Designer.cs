﻿namespace Estudiante
{
    partial class EstG1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dgvM1 = new System.Windows.Forms.DataGridView();
            this.lblg1 = new System.Windows.Forms.Label();
            this.lblPNombre = new System.Windows.Forms.Label();
            this.lblSNombre = new System.Windows.Forms.Label();
            this.lblGrupo = new System.Windows.Forms.Label();
            this.lblCarnet = new System.Windows.Forms.Label();
            this.lblPApellido = new System.Windows.Forms.Label();
            this.lblSApellido = new System.Windows.Forms.Label();
            this.txtPNombre = new System.Windows.Forms.TextBox();
            this.txtPApellido = new System.Windows.Forms.TextBox();
            this.txtSNombre = new System.Windows.Forms.TextBox();
            this.txtSApellido = new System.Windows.Forms.TextBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.mtxtGrupo = new System.Windows.Forms.MaskedTextBox();
            this.mtxtCarnet = new System.Windows.Forms.MaskedTextBox();
            this.lblFisica = new System.Windows.Forms.Label();
            this.lblBase = new System.Windows.Forms.Label();
            this.lblFinanzas = new System.Windows.Forms.Label();
            this.lblMicro = new System.Windows.Forms.Label();
            this.lblEstad = new System.Windows.Forms.Label();
            this.txtFisica = new System.Windows.Forms.TextBox();
            this.txtMicro = new System.Windows.Forms.TextBox();
            this.txtBaseD = new System.Windows.Forms.TextBox();
            this.txtFinanzas = new System.Windows.Forms.TextBox();
            this.txtEstad = new System.Windows.Forms.TextBox();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnClearTextBox = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvM1)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvM1
            // 
            this.dgvM1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvM1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvM1.Location = new System.Drawing.Point(309, 333);
            this.dgvM1.Name = "dgvM1";
            this.dgvM1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvM1.Size = new System.Drawing.Size(610, 252);
            this.dgvM1.TabIndex = 0;
            this.dgvM1.Click += new System.EventHandler(this.dgvM1_Click);
            // 
            // lblg1
            // 
            this.lblg1.AutoSize = true;
            this.lblg1.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblg1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblg1.Location = new System.Drawing.Point(250, 54);
            this.lblg1.Name = "lblg1";
            this.lblg1.Size = new System.Drawing.Size(619, 36);
            this.lblg1.TabIndex = 1;
            this.lblg1.Text = "GRUPO 2M1 INGENIERIA DE SISTEMAS";
            // 
            // lblPNombre
            // 
            this.lblPNombre.AutoSize = true;
            this.lblPNombre.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPNombre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPNombre.Location = new System.Drawing.Point(235, 112);
            this.lblPNombre.Name = "lblPNombre";
            this.lblPNombre.Size = new System.Drawing.Size(113, 15);
            this.lblPNombre.TabIndex = 2;
            this.lblPNombre.Text = "PRIMER NOMBRE";
            // 
            // lblSNombre
            // 
            this.lblSNombre.AutoSize = true;
            this.lblSNombre.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSNombre.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSNombre.Location = new System.Drawing.Point(226, 170);
            this.lblSNombre.Name = "lblSNombre";
            this.lblSNombre.Size = new System.Drawing.Size(125, 15);
            this.lblSNombre.TabIndex = 3;
            this.lblSNombre.Text = "SEGUNDO NOMBRE";
            // 
            // lblGrupo
            // 
            this.lblGrupo.AutoSize = true;
            this.lblGrupo.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGrupo.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblGrupo.Location = new System.Drawing.Point(735, 114);
            this.lblGrupo.Name = "lblGrupo";
            this.lblGrupo.Size = new System.Drawing.Size(51, 15);
            this.lblGrupo.TabIndex = 4;
            this.lblGrupo.Text = "GRUPO";
            // 
            // lblCarnet
            // 
            this.lblCarnet.AutoSize = true;
            this.lblCarnet.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCarnet.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblCarnet.Location = new System.Drawing.Point(735, 173);
            this.lblCarnet.Name = "lblCarnet";
            this.lblCarnet.Size = new System.Drawing.Size(58, 15);
            this.lblCarnet.TabIndex = 5;
            this.lblCarnet.Text = "CARNET";
            // 
            // lblPApellido
            // 
            this.lblPApellido.AutoSize = true;
            this.lblPApellido.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPApellido.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblPApellido.Location = new System.Drawing.Point(471, 117);
            this.lblPApellido.Name = "lblPApellido";
            this.lblPApellido.Size = new System.Drawing.Size(120, 15);
            this.lblPApellido.TabIndex = 6;
            this.lblPApellido.Text = "PRIMER APELLIDO";
            // 
            // lblSApellido
            // 
            this.lblSApellido.AutoSize = true;
            this.lblSApellido.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSApellido.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblSApellido.Location = new System.Drawing.Point(471, 168);
            this.lblSApellido.Name = "lblSApellido";
            this.lblSApellido.Size = new System.Drawing.Size(132, 15);
            this.lblSApellido.TabIndex = 7;
            this.lblSApellido.Text = "SEGUNDO APELLIDO";
            // 
            // txtPNombre
            // 
            this.txtPNombre.Location = new System.Drawing.Point(354, 112);
            this.txtPNombre.Name = "txtPNombre";
            this.txtPNombre.Size = new System.Drawing.Size(100, 20);
            this.txtPNombre.TabIndex = 11;
            this.txtPNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPNombre_KeyPress);
            // 
            // txtPApellido
            // 
            this.txtPApellido.Location = new System.Drawing.Point(612, 115);
            this.txtPApellido.Name = "txtPApellido";
            this.txtPApellido.Size = new System.Drawing.Size(100, 20);
            this.txtPApellido.TabIndex = 16;
            this.txtPApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPApellido_KeyPress);
            // 
            // txtSNombre
            // 
            this.txtSNombre.Location = new System.Drawing.Point(354, 168);
            this.txtSNombre.Name = "txtSNombre";
            this.txtSNombre.Size = new System.Drawing.Size(100, 20);
            this.txtSNombre.TabIndex = 17;
            this.txtSNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSNombre_KeyPress);
            // 
            // txtSApellido
            // 
            this.txtSApellido.Location = new System.Drawing.Point(612, 168);
            this.txtSApellido.Name = "txtSApellido";
            this.txtSApellido.Size = new System.Drawing.Size(100, 20);
            this.txtSApellido.TabIndex = 18;
            this.txtSApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtSApellido_KeyPress);
            // 
            // mtxtGrupo
            // 
            this.mtxtGrupo.Location = new System.Drawing.Point(811, 115);
            this.mtxtGrupo.Mask = "0L0";
            this.mtxtGrupo.Name = "mtxtGrupo";
            this.mtxtGrupo.Size = new System.Drawing.Size(65, 20);
            this.mtxtGrupo.TabIndex = 19;
            this.mtxtGrupo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // mtxtCarnet
            // 
            this.mtxtCarnet.Location = new System.Drawing.Point(811, 168);
            this.mtxtCarnet.Mask = "0000-0000L";
            this.mtxtCarnet.Name = "mtxtCarnet";
            this.mtxtCarnet.Size = new System.Drawing.Size(81, 20);
            this.mtxtCarnet.TabIndex = 20;
            this.mtxtCarnet.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblFisica
            // 
            this.lblFisica.AutoSize = true;
            this.lblFisica.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFisica.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFisica.Location = new System.Drawing.Point(292, 262);
            this.lblFisica.Name = "lblFisica";
            this.lblFisica.Size = new System.Drawing.Size(48, 15);
            this.lblFisica.TabIndex = 21;
            this.lblFisica.Text = "FISICA";
            // 
            // lblBase
            // 
            this.lblBase.AutoSize = true;
            this.lblBase.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBase.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblBase.Location = new System.Drawing.Point(260, 288);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(115, 15);
            this.lblBase.TabIndex = 22;
            this.lblBase.Text = "BASE DE DATOS 1";
            // 
            // lblFinanzas
            // 
            this.lblFinanzas.AutoSize = true;
            this.lblFinanzas.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFinanzas.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblFinanzas.Location = new System.Drawing.Point(538, 257);
            this.lblFinanzas.Name = "lblFinanzas";
            this.lblFinanzas.Size = new System.Drawing.Size(70, 15);
            this.lblFinanzas.TabIndex = 23;
            this.lblFinanzas.Text = "FINANZAS";
            // 
            // lblMicro
            // 
            this.lblMicro.AutoSize = true;
            this.lblMicro.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMicro.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblMicro.Location = new System.Drawing.Point(515, 291);
            this.lblMicro.Name = "lblMicro";
            this.lblMicro.Size = new System.Drawing.Size(119, 15);
            this.lblMicro.TabIndex = 24;
            this.lblMicro.Text = "MICROECONOMIA";
            // 
            // lblEstad
            // 
            this.lblEstad.AutoSize = true;
            this.lblEstad.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstad.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.lblEstad.Location = new System.Drawing.Point(752, 262);
            this.lblEstad.Name = "lblEstad";
            this.lblEstad.Size = new System.Drawing.Size(89, 15);
            this.lblEstad.TabIndex = 25;
            this.lblEstad.Text = "ESTADISTICA";
            // 
            // txtFisica
            // 
            this.txtFisica.Location = new System.Drawing.Point(381, 257);
            this.txtFisica.MaxLength = 2;
            this.txtFisica.Name = "txtFisica";
            this.txtFisica.Size = new System.Drawing.Size(58, 20);
            this.txtFisica.TabIndex = 33;
            this.txtFisica.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFisica.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFisica_KeyPress);
            // 
            // txtMicro
            // 
            this.txtMicro.Location = new System.Drawing.Point(640, 291);
            this.txtMicro.MaxLength = 2;
            this.txtMicro.Name = "txtMicro";
            this.txtMicro.Size = new System.Drawing.Size(58, 20);
            this.txtMicro.TabIndex = 34;
            this.txtMicro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtMicro.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMicro_KeyPress);
            // 
            // txtBaseD
            // 
            this.txtBaseD.Location = new System.Drawing.Point(381, 289);
            this.txtBaseD.MaxLength = 2;
            this.txtBaseD.Name = "txtBaseD";
            this.txtBaseD.Size = new System.Drawing.Size(58, 20);
            this.txtBaseD.TabIndex = 35;
            this.txtBaseD.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtBaseD.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBaseD_KeyPress);
            // 
            // txtFinanzas
            // 
            this.txtFinanzas.Location = new System.Drawing.Point(640, 257);
            this.txtFinanzas.MaxLength = 2;
            this.txtFinanzas.Name = "txtFinanzas";
            this.txtFinanzas.Size = new System.Drawing.Size(58, 20);
            this.txtFinanzas.TabIndex = 36;
            this.txtFinanzas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtFinanzas.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFinanzas_KeyPress);
            // 
            // txtEstad
            // 
            this.txtEstad.Location = new System.Drawing.Point(847, 257);
            this.txtEstad.MaxLength = 2;
            this.txtEstad.Name = "txtEstad";
            this.txtEstad.Size = new System.Drawing.Size(58, 20);
            this.txtEstad.TabIndex = 37;
            this.txtEstad.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtEstad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEstad_KeyPress);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDelete.FlatAppearance.BorderSize = 0;
            this.btnDelete.Image = global::Estudiante.Properties.Resources.deleteuser;
            this.btnDelete.Location = new System.Drawing.Point(173, 459);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 65);
            this.btnDelete.TabIndex = 32;
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnAdd
            // 
            this.btnAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAdd.FlatAppearance.BorderSize = 0;
            this.btnAdd.Image = global::Estudiante.Properties.Resources.add;
            this.btnAdd.Location = new System.Drawing.Point(173, 372);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(75, 67);
            this.btnAdd.TabIndex = 31;
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnClearTextBox
            // 
            this.btnClearTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.btnClearTextBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnClearTextBox.FlatAppearance.BorderSize = 0;
            this.btnClearTextBox.Image = global::Estudiante.Properties.Resources.clean;
            this.btnClearTextBox.Location = new System.Drawing.Point(173, 538);
            this.btnClearTextBox.Name = "btnClearTextBox";
            this.btnClearTextBox.Size = new System.Drawing.Size(75, 65);
            this.btnClearTextBox.TabIndex = 38;
            this.btnClearTextBox.UseVisualStyleBackColor = false;
            this.btnClearTextBox.Click += new System.EventHandler(this.btnClearTextBox_Click);
            // 
            // EstG1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(66)))), ((int)(((byte)(82)))));
            this.ClientSize = new System.Drawing.Size(1086, 615);
            this.Controls.Add(this.btnClearTextBox);
            this.Controls.Add(this.txtEstad);
            this.Controls.Add(this.txtFinanzas);
            this.Controls.Add(this.txtBaseD);
            this.Controls.Add(this.txtMicro);
            this.Controls.Add(this.txtFisica);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.lblEstad);
            this.Controls.Add(this.lblMicro);
            this.Controls.Add(this.lblFinanzas);
            this.Controls.Add(this.lblBase);
            this.Controls.Add(this.lblFisica);
            this.Controls.Add(this.mtxtCarnet);
            this.Controls.Add(this.mtxtGrupo);
            this.Controls.Add(this.txtSApellido);
            this.Controls.Add(this.txtSNombre);
            this.Controls.Add(this.txtPApellido);
            this.Controls.Add(this.txtPNombre);
            this.Controls.Add(this.lblSApellido);
            this.Controls.Add(this.lblPApellido);
            this.Controls.Add(this.lblCarnet);
            this.Controls.Add(this.lblGrupo);
            this.Controls.Add(this.lblSNombre);
            this.Controls.Add(this.lblPNombre);
            this.Controls.Add(this.lblg1);
            this.Controls.Add(this.dgvM1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "EstG1";
            this.Text = "EstG1";
            ((System.ComponentModel.ISupportInitialize)(this.dgvM1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvM1;
        private System.Windows.Forms.Label lblg1;
        private System.Windows.Forms.Label lblPNombre;
        private System.Windows.Forms.Label lblSNombre;
        private System.Windows.Forms.Label lblGrupo;
        private System.Windows.Forms.Label lblCarnet;
        private System.Windows.Forms.Label lblPApellido;
        private System.Windows.Forms.Label lblSApellido;
        private System.Windows.Forms.TextBox txtPNombre;
        private System.Windows.Forms.TextBox txtPApellido;
        private System.Windows.Forms.TextBox txtSNombre;
        private System.Windows.Forms.TextBox txtSApellido;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.MaskedTextBox mtxtGrupo;
        private System.Windows.Forms.MaskedTextBox mtxtCarnet;
        private System.Windows.Forms.Label lblFisica;
        private System.Windows.Forms.Label lblBase;
        private System.Windows.Forms.Label lblFinanzas;
        private System.Windows.Forms.Label lblMicro;
        private System.Windows.Forms.Label lblEstad;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.TextBox txtFisica;
        private System.Windows.Forms.TextBox txtMicro;
        private System.Windows.Forms.TextBox txtBaseD;
        private System.Windows.Forms.TextBox txtFinanzas;
        private System.Windows.Forms.TextBox txtEstad;
        private System.Windows.Forms.Button btnClearTextBox;
    }
}