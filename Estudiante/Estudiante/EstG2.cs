﻿using Estudiante.Entities;
using Estudiante.Json;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Estudiante
{
    public partial class EstG2 : Form
    {
        //private static string ruta = @"C:\Users\Steven Espinoza P\Desktop\Compartida CASA\UNI\SEMESTRE 4\BASE DE DATOS 1\Diagnostico\Estudiante\Estudiante\Resources\2m2.json";
        private string ruta = @"C:\temp\2m2.json";
        private DataTable dt;
        private Universitario uni;
        private List<Universitario> universitarios;
        private List<Asignatura> materias;

        public EstG2()
        {
            InitializeComponent();
            dt = new DataTable();
            materias = new List<Asignatura>();
            universitarios = new List<Universitario>();
            universitarios = Populate.DeserealizarJson(ruta);
            if(universitarios == null)
            {
                universitarios = new List<Universitario>();
            }
            dt = Populate.DeserealizarToTable(ruta);
            if (dt != null )
            {
                dt.Columns.RemoveAt(6);
            }
            dgvM1.DataSource = dt;
            dgvM1.ClearSelection();
        }
        
              
        private void txtPNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloTexto(e);
        }

        private void soloTexto(KeyPressEventArgs evt)
        {
            if(Char.IsDigit(evt.KeyChar) || Char.IsNumber(evt.KeyChar) || Char.IsSeparator(evt.KeyChar) || Char.IsSymbol(evt.KeyChar))
            {
                evt.Handled = true;
            }
        }

        private void txtSNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloTexto(e);
        }

        private void txtPApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloTexto(e);
        }

        private void txtSApellido_KeyPress(object sender, KeyPressEventArgs e)
        {
            soloTexto(e);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(verificarVacio() == true)
            {
                MessageBox.Show("Alguno de los campos se encuentra vacio.");
                return;

            }
            CargarEstudiante();
            Populate.EscribirEnJson(ruta, universitarios);
            dt = null;
            dt = Populate.DeserealizarToTable(ruta);
            dgvM1.DataSource = dt;

        }

        private void CargarEstudiante()
        {
            string pNombre = txtPNombre.Text;
            string sNombre = txtSNombre.Text;
            string pApellido = txtPApellido.Text;
            string sApellido = txtSApellido.Text;
            string grupo = mtxtGrupo.Text;
            string carnet = mtxtCarnet.Text;

            materias.Add(new Asignatura(lblFisica.Text,Int32.Parse(txtFisica.Text)));
            materias.Add(new Asignatura(lblBase.Text,Int32.Parse(txtBaseD.Text)));
            materias.Add(new Asignatura(lblFinanzas.Text, Int32.Parse(txtFinanzas.Text)));
            materias.Add(new Asignatura(lblMicro.Text,Int32.Parse(txtMicro.Text)));
            materias.Add(new Asignatura(lblEstad.Text,Int32.Parse(txtEstad.Text)));
            
            uni = new Universitario(pNombre, sNombre, pApellido, sApellido, grupo, carnet, materias);

            universitarios.Add(uni);

            CleanCampos();
        }

        private void txtFisica_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtBaseD_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtFinanzas_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtMicro_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void txtEstad_KeyPress(object sender, KeyPressEventArgs e)
        {
            SoloNumeros(e);
        }

        private void SoloNumeros(KeyPressEventArgs evt)
        {
            if (Char.IsLetter(evt.KeyChar) || Char.IsSymbol(evt.KeyChar) || Char.IsSeparator(evt.KeyChar))
            {
                evt.Handled = true;
            }
        }

        private void dgvM1_Click(object sender, EventArgs e)
        {
            int n = dgvM1.CurrentCell.RowIndex;
            txtPNombre.Text = dgvM1.CurrentRow.Cells[0].Value.ToString();
            txtSNombre.Text = dgvM1.CurrentRow.Cells[1].Value.ToString();
            txtPApellido.Text = dgvM1.CurrentRow.Cells[2].Value.ToString();
            txtSApellido.Text = dgvM1.CurrentRow.Cells[3].Value.ToString();
            mtxtGrupo.Text = dgvM1.CurrentRow.Cells[4].Value.ToString();
            mtxtCarnet.Text = dgvM1.CurrentRow.Cells[5].Value.ToString();
            txtFisica.Text = universitarios.ElementAt(n).asignaturas.ElementAt(0).calificacion.ToString();
            txtBaseD.Text = universitarios.ElementAt(n).asignaturas.ElementAt(1).calificacion.ToString();
            txtFinanzas.Text = universitarios.ElementAt(n).asignaturas.ElementAt(2).calificacion.ToString();
            txtMicro.Text = universitarios.ElementAt(n).asignaturas.ElementAt(3).calificacion.ToString();
            txtEstad.Text = universitarios.ElementAt(n).asignaturas.ElementAt(4).calificacion.ToString();
        }

        private void btnClearTextBox_Click(object sender, EventArgs e)
        {
            CleanCampos();
        }
        private void CleanCampos()
        {
            txtFisica.Text = "";
            txtBaseD.Text = "";
            txtFinanzas.Text = "";
            txtMicro.Text = "";
            txtEstad.Text = "";
            txtPNombre.Text = "";
            txtSNombre.Text = "";
            txtPApellido.Text = "";
            txtSApellido.Text = "";
            mtxtCarnet.Text = "";
            mtxtGrupo.Text = "";
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int n = dgvM1.CurrentCell.RowIndex;
            universitarios.RemoveAt(n);
            //dgvM1.Rows.RemoveAt(n);
            Populate.EscribirEnJson(ruta, universitarios);
            dt = null;
            dt = Populate.DeserealizarToTable(ruta);
            dgvM1.DataSource = dt;
        }

        public bool verificarVacio()
        {
            if (String.IsNullOrEmpty(txtPNombre.Text) || String.IsNullOrEmpty(txtSNombre.Text) || String.IsNullOrEmpty(txtPApellido.Text) || String.IsNullOrEmpty(txtSApellido.Text) || String.IsNullOrEmpty(mtxtCarnet.Text) || String.IsNullOrEmpty(mtxtGrupo.Text) || String.IsNullOrEmpty(txtFinanzas.Text) || String.IsNullOrEmpty(txtFisica.Text) || String.IsNullOrEmpty(txtBaseD.Text) || String.IsNullOrEmpty(txtMicro.Text) || String.IsNullOrEmpty(txtEstad.Text))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
