﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace Estudiante
{
    public partial class Forma : Form
    {
        public Forma()
        {
            InitializeComponent();
        }

        private void picExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void picMax_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            picMax.Visible = false;
            picRes.Visible = true;
        }

        private void picRes_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Normal;
            picRes.Visible = false;
            picMax.Visible = true;
        }

        private void picMin_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int IParam);

        private void pnlTitulo_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
        }

        private void horaFecha_Tick(object sender, EventArgs e)
        {
            lblHora.Text = DateTime.Now.ToLongTimeString();
            lblFecha.Text = DateTime.Now.ToShortDateString();
        }

        private void btnEstudiantes_Click(object sender, EventArgs e)
        {
            pnlSubEst.Visible = true;
        }

        private void btn2m1_Click(object sender, EventArgs e)
        {
            pnlSubEst.Visible = false;
            AbrirFrmHijo(new EstG1());
        }

        private void btn2m2_Click(object sender, EventArgs e)
        {
            pnlSubEst.Visible = false;
            AbrirFrmHijo(new EstG2());
        }

        private void AbrirFrmHijo(object frm) {
            if (this.pnlContenido.Controls.Count > 0)
            {
                this.pnlContenido.Controls.RemoveAt(0);
            }

            Form fh = frm as Form;
            fh.TopLevel = false;
            fh.Dock = DockStyle.Fill;
            this.pnlContenido.Controls.Add(fh);
            this.pnlContenido.Tag = fh;
            fh.Show();
        }
        
         
        private void picHome_Click(object sender, EventArgs e)
        {
            AbrirFrmHijo(new Home());
        }

        private void Forma_Load(object sender, EventArgs e)
        {
            picHome_Click(null, e);
        }
    }

    }
        
    

