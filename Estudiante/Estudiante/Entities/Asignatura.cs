﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudiante.Entities
{
    public class Asignatura
    {
        public string nombre { get; set; }
        public int calificacion { get; set; }

        public Asignatura()
        {
        }

        public Asignatura(string nombre, int calificacion)
        {
            this.nombre = nombre;
            this.calificacion = calificacion;
        }
    }
}
