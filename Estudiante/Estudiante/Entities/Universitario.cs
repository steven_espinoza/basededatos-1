﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudiante.Entities
{
    public class Universitario
    {
        public string PrimerNombre { get; set; }
        public string SegundoNombre { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public string Grupo { get; set; }
        public string Carnet { get; set; }
        public List<Asignatura> asignaturas { get; set; }

        public Universitario()
        {
        }

        public Universitario(string primerNombre, string segundoNombre, string primerApellido, string segundoApellido, string grupo, string carnet, List<Asignatura> asignaturas)
        {
            PrimerNombre = primerNombre;
            SegundoNombre = segundoNombre;
            PrimerApellido = primerApellido;
            SegundoApellido = segundoApellido;
            Grupo = grupo;
            Carnet = carnet;
            this.asignaturas = asignaturas;
        }
    }
}
