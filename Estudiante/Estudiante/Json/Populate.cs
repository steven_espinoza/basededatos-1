﻿using Estudiante.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estudiante.Json
{
    public static class Populate
    {
        public static void EscribirEnJson(string path,List<Universitario> std)
        {
            //File.Exists(path);
            string studentsJson = JsonConvert.SerializeObject(std.ToArray(), Formatting.Indented);
            File.WriteAllText(path, studentsJson);
        }

        public static List<Universitario> DeserealizarJson(string path)
        {
                if (!File.Exists(path))
                {
                    File.Create(path).Close();
                    
                }
                string contactsfrom;
                using (var reader = new StreamReader(path))
                {
                    contactsfrom = reader.ReadToEnd();
                }
                List<Universitario> u = JsonConvert.DeserializeObject<List<Universitario>>(contactsfrom);

                return u;
        }

        public static DataTable DeserealizarToTable(string path)
        {
            
                string contactsfrom;
                using (var reader = new StreamReader(path))
                {
                    contactsfrom = reader.ReadToEnd();
                }

                var dat = (DataTable)JsonConvert.DeserializeObject(contactsfrom, typeof(DataTable));

                return dat;
            
        }
    }
}
